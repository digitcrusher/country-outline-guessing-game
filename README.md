# Country Outline Guessing Game

As you might have guessed from the name, this is a simple browser game in which you have to guess the names of countries based on their shapes. It uses map data from [Natural Earth](https://www.naturalearthdata.com/). I got the inspiration to make this because I couldn't find any such games online following [Ba Ba Dum](https://babadum.com/)'s format. The easiest way to play this game is through [my website](https://lacina.io/country-outline-guessing-game/) and a harder way to do that is to:

1. Make sure you have Python 3 installed.
2. Install the packages "pyproj", "pyshp" and "shapely" using `pip3 install -r render_requirements.txt`.
3. `./download_maps.sh`
4. `./render_earth.py`
5. `./render_countries.py`
6. Run a local HTTP server in this directory on your computer, e.g. using `python3 -m http.server`, or upload `index.html` and `assets` to your remote HTTP server.
