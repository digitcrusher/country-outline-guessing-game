#!/usr/bin/env python3

import itertools, json, math, os, pyproj, shapefile, shapely.geometry
from pyproj import Transformer
from shapely.ops import transform

sf = shapefile.Reader('ne_10m_admin_0_countries.zip')

os.mkdir('assets/countries')

langs = []
for field in sf.fields:
  if field[0].startswith('NAME_') and len(field[0]) == 7:
    langs.append(field[0].removeprefix('NAME_').lower())

index = {}
for country in sf:
  code = country.record['ADM0_A3'].lower()
  print(code)

  shape = shapely.geometry.shape(country.shape)

  # https://theconversation.com/wheres-your-county-seat-a-modern-mathematical-method-for-calculating-centers-of-geography-71060
  center = shape.centroid
  azimuthal = Transformer.from_crs('EPSG:4326', {'proj': 'aeqd', 'lon_0': center.x, 'lat_0': center.y}, always_xy=True)
  center = transform(lambda x, y: azimuthal.transform(x, y, direction=pyproj.enums.TransformDirection.INVERSE), transform(azimuthal.transform, shape).centroid)

  orthographic = Transformer.from_crs('EPSG:4326', {'proj': 'ortho', 'lon_0': center.x, 'lat_0': center.y}, always_xy=True)
  shape = transform(orthographic.transform, shape)
  if isinstance(shape, shapely.geometry.MultiPolygon):
    geoms = list(shape.geoms)
    i = 0
    while i < len(geoms):
      for x, y in geoms[i].exterior.coords:
        if math.isinf(x) or math.isinf(y):
          del geoms[i]
          i -= 1
          break
      i += 1
    shape = shapely.geometry.MultiPolygon(geoms)
  #shape = shape.buffer(0.001, join_style=shapely.geometry.JOIN_STYLE.mitre) # .buffer() merges land masses separated by the antimeridian such as Antarctica.

  shape = transform(lambda x, y: (x, -y), shape)
  aspect_ratio = 3 / 2 # Same as in style.css
  size = max((shape.bounds[2] - shape.bounds[0]) / aspect_ratio, shape.bounds[3] - shape.bounds[1])
  #shape = shape.buffer(size / 1000.0).simplify(size / 500.0) # More coastline detail
  shape = shape.buffer(size / 500.0).simplify(size / 250.0) # Blockier and more consistent with tiny countries

  with open(f'assets/countries/{code}.svg', 'x') as svg:
    minx = shape.bounds[0]
    miny = shape.bounds[1]
    width = shape.bounds[2] - minx
    height = shape.bounds[3] - miny
    size = max(width / aspect_ratio, height)
    cliff_height = size / 75.0
    height += cliff_height

    # Changing the color of an external SVG in CSS is not so straightforward either.
    svg.write(f'<svg width="{width}" height="{height}" viewBox="{minx} {miny} {width} {height}" xmlns="http://www.w3.org/2000/svg" fill="mediumseagreen">')

    svg.write('<filter id="cliff">') # This is the same as putting a #00000030 overlay on top of the cliffs.
    svg.write('<feComponentTransfer color-interpolation-filters="sRGB">')
    svg.write('<feFuncR type="linear" slope="0.8125"/>')
    svg.write('<feFuncG type="linear" slope="0.8125"/>')
    svg.write('<feFuncB type="linear" slope="0.8125"/>')
    svg.write('</feComponentTransfer>')
    svg.write('</filter>')
    svg.write('<g filter="url(#cliff)">')

    def add_cliff(ring, is_interior=False):
      cliff = []
      def flush():
        nonlocal cliff
        if len(cliff) > 1:
          svg.write(f'M{cliff[0][0]},{cliff[0][1] - size / 500.0}')
          for x, y in cliff[1:]:
            svg.write(f'L{x},{y - size / 500.0}') # Fixes a pixel gap between the cliffs and the land.
          for x, y in cliff[::-1]:
            svg.write(f'L{x},{y + cliff_height}')
        cliff = []

      if ring.is_ccw == is_interior:
        for point, next in itertools.pairwise(ring.coords):
          cliff.append(point)
          if next[0] <= point[0]:
            flush()
      else:
        for point, next in itertools.pairwise(ring.coords):
          cliff.append(point)
          if next[0] >= point[0]:
            flush()
      cliff.append(ring.coords[0])
      flush()

    geoms = shape.geoms if isinstance(shape, shapely.geometry.MultiPolygon) else [shape]
    for geom in geoms:
      svg.write(f'<path d="')
      add_cliff(geom.exterior)
      for ring in geom.interiors:
        add_cliff(ring, True)
      svg.write('"/>')

    svg.write('</g>')

    svg.write('<g>')
    svg.write(shape.svg(scale_factor=0.0, fill_color='', opacity=1.0).replace(' fill="" stroke="#555555" stroke-width="0.0" opacity="1.0"', '').replace('<g>', '').replace('</g>', '').replace(' />', '/>').replace('M ', 'M').replace(' L ', 'L').replace(' z ', '').replace(' z', ''))
    svg.write('</g>')

    svg.write('</svg>\n')

  index[code] = {}
  for lang in langs:
    index[code][lang] = country.record['NAME_' + lang.upper()]

override = json.load(open('countries_index_override.json', 'r'))
for country, names in override.items():
  index[country].update(names)

json.dump(index, open('assets/countries/index.json', 'x'), ensure_ascii=False, separators=(',', ':'))
