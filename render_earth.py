#!/usr/bin/env python3

import shapefile, shapely.geometry, shapely.ops

sf = shapefile.Reader('ne_110m_land.zip')
earth = shapely.geometry.Point()
for shape in sf.iterShapes():
  earth = earth.union(shapely.geometry.shape(shape))
earth = shapely.ops.transform(lambda x, y: (x, -y), earth)
with open('assets/earth.svg', 'x') as svg:
  # Unfortunately, you cannot change the fill color of an SVG drawn on a canvas easily.
  svg.write(f'<svg width="360" height="180" viewBox="-180 -90 360 180" xmlns="http://www.w3.org/2000/svg" fill="mediumseagreen">')
  svg.write(earth.svg(scale_factor=0.0, fill_color='', opacity=1.0).replace(' fill="" stroke="#555555" stroke-width="0.0" opacity="1.0"', '').replace('<g>', '').replace('</g>', '').replace(' />', '/>').replace('M ', 'M').replace(' L ', 'L').replace(' z ', '').replace(' z', ''))
  svg.write('</svg>\n')
