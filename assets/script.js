'use strict';

function newShaderStage(gl, source, type) {
  const result = gl.createShader(type);
  gl.shaderSource(result, source);
  gl.compileShader(result);

  if(!gl.getShaderParameter(result, gl.COMPILE_STATUS)) {
    alert(`Failed to compile shader stage:\n${gl.getShaderInfoLog(result)}`);
    gl.deleteShader(result);
    return null;
  }

  return result;
}

function newShader(gl, vertexShader, fragShader) {
  const vertexStage = newShaderStage(gl, vertexShader, gl.VERTEX_SHADER);
  const fragStage = newShaderStage(gl, fragShader, gl.FRAGMENT_SHADER);

  if(vertexStage === null || fragStage === null) {
    gl.deleteShader(vertexStage);
    gl.deleteShader(fragStage);
    return null;
  }

  const result = gl.createProgram();
  gl.attachShader(result, vertexStage);
  gl.attachShader(result, fragStage);
  gl.linkProgram(result);

  if(!gl.getProgramParameter(result, gl.LINK_STATUS)) {
    alert(`Failed to link shader:\n${gl.getProgramInfoLog(result)}`);
    gl.deleteProgram(result);
    return null;
  }

  return result;
}

function getElementsByIds(...ids) {
  const result = [];
  for(const id of ids) {
    result.push(document.getElementById(id));
  }
  return result;
}

function addClassToElems(elems, className) {
  for(const elem of elems) {
    elem.classList.add(className);
  }
}

function removeClassFromElems(elems, className) {
  for(const elem of elems) {
    elem.classList.remove(className);
  }
}

function replaceClassInElems(elems, oldClass, newClass) {
  for(const elem of elems) {
    elem.classList.replace(oldClass, newClass);
  }
}

function waitForTransitions(elems, then) {
  let counter = 0;
  function begin(event) {
    if(event.target !== this) return;
    counter++;
  }
  function end(event) {
    if(event.target !== this) return;
    counter--;
    if(counter === 0 && then !== null) {
      then();
      then = null;
    }
  }
  if(elems instanceof HTMLElement) {
    elems = [elems];
  }
  for(const elem of elems) {
    elem.ontransitionrun = begin;
    elem.ontransitionend = end;
  }
}

function randomChoice(array) {
  return array[Math.floor(Math.random() * array.length)];
}

function randomPop(array) {
  const idx = Math.floor(Math.random() * array.length);
  const result = array[idx];
  array.splice(idx, 1);
  return result;
}

let langs = null, countryNames = null;

window.onload = function() {
  if(initEarth()) return;

  let request = new XMLHttpRequest();
  request.open('GET', 'assets/langs/index.json');
  request.responseType = 'json';
  request.onload = function() {
    if(this.status !== 200) {
      alert(`Failed to download list of languages: ${this.status}`);
    } else if(this.response === null) {
      alert('Failed to download list of languages');
    } else {
      langs = this.response;
      populateLangSelect();
      setLang(window.localStorage.lang || 'en');
      tryToStart();
    }
  };
  request.send();

  request = new XMLHttpRequest();
  request.open('GET', 'assets/countries/index.json');
  request.responseType = 'json';
  request.onload = function() {
    if(this.status !== 200) {
      alert(`Failed to download list of countries: ${this.status}`);
    } else if(this.response === null) {
      alert('Failed to download list of countries');
    } else {
      countryNames = this.response;
      tryToStart();
    }
  };
  request.send();

  const playButton = document.getElementById('play');
  playButton.onclick = releasePlayButton;
  if(!window.chrome) { // Unfocusing a button breaks :active in Chrome.
    playButton.onfocus = function() {
      this.blur();
    };
  }

  const scoreOuter = document.getElementById('score'), scoreInner = document.querySelector('#score > div');
  scoreInner.onmousedown = scoreInner.ontouchstart = function() {
    scoreOuter.classList.add('pressed');
  };
  scoreInner.onmouseup = function() {
    if(scoreOuter.classList.contains('points')) {
      scoreOuter.classList.remove('points');
      scoreOuter.classList.add('accuracy');
    } else {
      scoreOuter.classList.remove('accuracy');
      scoreOuter.classList.add('points');
    }
    updateScore();
  };
  window.onmouseup = window.ontouchend = function() {
    scoreOuter.classList.remove('pressed');
  };

  document.querySelector('#lang-select select').onchange = function() {
    setLang(this.value);
  };

  for(const button of document.querySelectorAll('#choices button')) {
    button.onclick = function() {
      releaseChoiceButton(this);
    };
    if(!window.chrome) {
      button.onfocus = function() {
        this.blur();
      };
    }
  }

  window.onkeyup = function(event) {
    if(event.key === 'Escape') {
      if(scene !== 'game') return;
      enterTitle();
    } else if(event.key === 'Enter') {
      releasePlayButton();
    } else if(parseInt(event.key).toString() === event.key) {
      const buttons = document.querySelectorAll('#choices button');
      if(event.key - 1 in buttons) {
        releaseChoiceButton(buttons[event.key - 1]);
      }
    }
  };
  window.onkeydown = function(event) {
    if(event.key === 'Enter') {
      pressPlayButton();
    } else if(parseInt(event.key).toString() === event.key) {
      const buttons = document.querySelectorAll('#choices button');
      if(event.key - 1 in buttons) {
        pressChoiceButton(buttons[event.key - 1]);
      }
    }
  };

  (window.onresize = function() {
    document.body.style.height = window.innerHeight + 'px';
    updateChoicesScale();
  })();

  const render = function(timestamp) {
    if(isSurfaceImgLoaded) {
      drawEarth(timestamp / 1000);
    }
    window.requestAnimationFrame(render);
  };
  window.requestAnimationFrame(render);
};

let squareVbo = null, earthShader = null, surfaceTex = null, surfaceImg = null, isSurfaceImgLoaded = false;

function initEarth() {
  const gl = document.querySelector('#earth canvas').getContext('webgl', { premultipliedAlpha: false });
  if(gl === null) {
    alert("Your browser doesn't support WebGL.");
    return true;
  }

  squareVbo = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, squareVbo);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
    -1.0, -1.0,
     1.0, -1.0,
     1.0,  1.0,
    -1.0,  1.0,
  ]), gl.STATIC_DRAW);

  const earthVertexShader = `
    attribute vec2 position;

    uniform vec2 canvas_size;
    uniform float diameter;

    varying vec2 frag_pos;

    void main() {
      gl_Position = vec4(position / canvas_size * diameter, 0.0, 1.0);
      frag_pos = position;
    }
  `;

  const earthFragShader = `
    #define PI 3.14159265358979323846264338327950288419716939937510
    precision highp float;

    varying vec2 frag_pos;

    uniform float diameter;
    uniform float dist; // …between the viewer and the surface in radii
    uniform float turns;
    uniform sampler2D surface;

    void main() {
      float viewer_to_horizon = sqrt(dist * dist + 2.0 * dist);
      float horizon_arc = 2.0 * atan(viewer_to_horizon);
      float horizon_radius = viewer_to_horizon / (dist + 1.0);
      // This here is the only uncertain equation out of all.
      float distortion = (dist + 1.0 - sqrt(1.0 - length(frag_pos * horizon_arc / PI / 2.0))) / (dist + 1.0 - sqrt(1.0 - horizon_arc / PI / 2.0));
      if(dist == 0.0) {
        distortion = 1.0;
      }
      float plane_x = frag_pos.x * distortion * horizon_radius;
      float plane_y = frag_pos.y * distortion * horizon_radius;
      float latitude = asin(plane_y);
      float longitude = PI / 2.0 + atan(plane_x / sqrt(1.0 - plane_x * plane_x - plane_y * plane_y));
      float tex_x = (longitude / PI + 0.5) / 2.0 - turns;
      float tex_y = latitude / PI + 0.5;
      float alpha = smoothstep(1.0, 1.0 - 2.0 / diameter, length(frag_pos));
      gl_FragColor = vec4(texture2D(surface, vec2(mod(tex_x, 1.0), tex_y)).rgb, alpha);
    }
  `;

  earthShader = newShader(gl, earthVertexShader, earthFragShader);
  if(earthShader === null) {
    return true;
  }

  surfaceTex = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, surfaceTex);
  gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);

  surfaceImg = new Image();
  surfaceImg.onload = function() {
    drawSurface();
    isSurfaceImgLoaded = true;
    tryToStart();
  };
  surfaceImg.onerror = function() {
    alert("Failed to load Earth's surface.");
  };
  surfaceImg.src = 'assets/earth.svg';

  return false;
}

function drawEarth(time) {
  const canvas = document.querySelector('#earth canvas');
  const old_width = canvas.width, old_height = canvas.height;
  canvas.width = canvas.parentElement.clientWidth * window.devicePixelRatio;
  canvas.height = canvas.parentElement.clientHeight * window.devicePixelRatio;
  if(canvas.width !== old_width || canvas.height !== old_height) {
    drawSurface();
  }

  const gl = canvas.getContext('webgl');
  gl.viewport(0, 0, canvas.width, canvas.height);
  gl.clearColor(0.0, 0.0, 0.0, 0.0);
  gl.clear(gl.COLOR_BUFFER_BIT);

  gl.bindBuffer(gl.ARRAY_BUFFER, squareVbo);
  const location = gl.getAttribLocation(earthShader, 'position');
  gl.vertexAttribPointer(location, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(location);

  gl.useProgram(earthShader);
  gl.uniform2f(gl.getUniformLocation(earthShader, 'canvas_size'), canvas.width, canvas.height);
  gl.uniform1f(gl.getUniformLocation(earthShader, 'diameter'), Math.min(canvas.width, canvas.height));
  gl.uniform1f(gl.getUniformLocation(earthShader, 'dist'), 2.0);
  gl.uniform1f(gl.getUniformLocation(earthShader, 'turns'), time / 20.0);
  gl.bindTexture(gl.TEXTURE_2D, surfaceTex);
  gl.uniform1i(gl.getUniformLocation(earthShader, 'surface'), 0);
  gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
}

function drawSurface() {
  const canvas = document.querySelector('#earth canvas');

  const offscreen = document.createElement('canvas');
  const diameter = Math.min(canvas.width, canvas.height) * 1.5;
  offscreen.width = diameter * 2;
  offscreen.height = diameter;
  const ctx = offscreen.getContext('2d');
  ctx.fillStyle = 'royalblue';
  ctx.fillRect(0, 0, offscreen.width, offscreen.height);
  ctx.drawImage(surfaceImg, 0, 0, offscreen.width, offscreen.height);

  const gl = canvas.getContext('webgl');
  gl.bindTexture(gl.TEXTURE_2D, surfaceTex);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, offscreen);
}

function populateLangSelect() {
  const select = document.querySelector('#lang-select select');
  select.textContent = '';
  for(const lang in langs) {
    const option = document.createElement('option');
    option.value = lang;
    option.textContent = langs[lang].name;
    select.appendChild(option);
  }
  document.getElementById('lang-select').style.removeProperty('display');
}

function setLang(value) {
  window.localStorage.lang = value;
  document.documentElement.lang = value;
  document.getElementById('play').textContent = langs[value].play;
  document.querySelector('#lang-select img').src = `assets/langs/${value}.svg`;
  document.querySelector('#lang-select select').value = value;
  for(const choice of document.querySelectorAll('#choices button')) {
    try {
      choice.textContent = countryNames[choice.value][value];
    } catch(error) {}
  }
  updateChoicesScale();
}

function updateChoicesScale() {
  const outer = document.getElementById('choices'), inner = document.querySelector('#choices > div');
  inner.style.scale = Math.min(outer.clientHeight / inner.clientHeight, 1);
}

function updateScore(change = 0) {
  const score = Math.max(0, (parseInt(window.localStorage.score) || 0) + change);
  const correctc = (parseInt(window.sessionStorage.correctc) || 0) + Math.max(0, change);
  const wrongc = (parseInt(window.sessionStorage.wrongc) || 0) + Math.max(0, -change);
  window.localStorage.score = score;
  window.sessionStorage.correctc = correctc;
  window.sessionStorage.wrongc = wrongc;

  const outer = document.getElementById('score'), inner = document.querySelector('#score > div');
  inner.textContent = '?';
  if(outer.classList.contains('points')) {
    inner.textContent = score;
  } else if(outer.classList.contains('accuracy')) {
    inner.textContent = (correctc + wrongc === 0 ? 100.0 : correctc * 100.0 / (correctc + wrongc)).toFixed(1);
  }
  outer.style.setProperty('--height', outer.clientHeight + 'px');
}

let scene = null;

function tryToStart() {
  if(scene === null && langs !== null && countryNames !== null && isSurfaceImgLoaded) {
    enterTitle();
  }
}

function enterTitle() {
  if(scene === null) {
    addClassToElems(getElementsByIds('earth', 'title-menu'), 'enter-title');
    scene = 'title';

  } else if(scene === 'game') {
    const elems = getElementsByIds('earth', 'score', 'country', 'choices');
    waitForTransitions(elems, function() {
      removeClassFromElems(elems, 'leave-game');
      scene = null;
      setTimeout(enterTitle, 0); // Wait for #earth's scale: 0;
    });
    replaceClassInElems(elems, 'enter-game', 'leave-game');
    scene = 'leaving';
  }
}

function pressPlayButton() {
  if(scene !== 'title') return;
  document.getElementById('play').classList.add('pressed');
}

function releasePlayButton() {
  document.getElementById('play').classList.remove('pressed');
  if(scene !== 'title') return;
  enterGame();
}

function enterGame() {
  updateScore();

  if(scene === null) {
    const elems = getElementsByIds('earth', 'score');
    waitForTransitions(elems, function() {
      if(scene !== 'game') return;
      prepareChoices();
    });
    addClassToElems(elems, 'enter-game');
    scene = 'game';

  } else if(scene === 'title') {
    const elems = getElementsByIds('earth', 'title-menu');
    waitForTransitions(elems, function() {
      removeClassFromElems(elems, 'leave-title');
      scene = null;
      enterGame();
    });
    replaceClassInElems(elems, 'enter-title', 'leave-title');
    scene = 'leaving';
  }
}

function prepareChoices() {
  const choices = document.getElementById('choices');
  choices.className = '';
  const buttons = choices.getElementsByTagName('button');
  for(const button of buttons) {
    button.textContent = '';
  }

  const availableCountries = Object.keys(countryNames);

  let answer = null;
  while(answer === null) {
    const country = randomPop(availableCountries);
    if(window.localStorage.lang in countryNames[country]) {
      answer = country;
    }
  }
  const correctButton = randomChoice(buttons);
  correctButton.textContent = countryNames[answer][window.localStorage.lang];
  correctButton.className = 'correct';
  correctButton.value = answer;

  for(const button of buttons) {
    while(button.textContent === '') {
      const country = randomPop(availableCountries);
      if(window.localStorage.lang in countryNames[country]) {
        button.textContent = countryNames[country][window.localStorage.lang];
        button.className = 'wrong';
        button.value = country;
      }
    }
  }
  updateChoicesScale();

  const img = document.querySelector('#country img');
  img.onload = function() {
    if(scene !== 'game') return;
    addClassToElems(getElementsByIds('country', 'choices'), 'enter-game');
  };
  img.onerror = function() {
    if(scene !== 'game') return;
    prepareChoices();
  };
  img.src = `assets/countries/${answer}.svg`;
}

function pressChoiceButton(button) {
  const choices = document.getElementById('choices');
  if(scene !== 'game' || choices.classList.contains('revealed')) return;
  button.classList.add('pressed');
}

function releaseChoiceButton(button) {
  const choices = document.getElementById('choices');
  if(scene !== 'game' || choices.classList.contains('revealed')) return;

  choices.classList.add('revealed');
  for(const button of choices.getElementsByTagName('button')) {
    button.classList.remove('pressed');
  }
  button.classList.add('pressed');

  updateScore(button.classList.contains('correct') ? 1 : -1);
  const score = document.getElementById('score');
  waitForTransitions(score, function() {
    score.classList.remove('changed');
  });
  score.classList.add('changed');

  setTimeout(function() {
    if(scene !== 'game') return;
    const elems = getElementsByIds('country', 'choices');
    waitForTransitions(elems, function() {
      if(scene !== 'game') return;
      removeClassFromElems(elems, 'leave-game');
      prepareChoices();
    });
    replaceClassInElems(elems, 'enter-game', 'leave-game');
  }, 1500);
}
